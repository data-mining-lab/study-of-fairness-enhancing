import numpy

from metrics.Accuracy import Accuracy
from metrics.SensitiveMetric import SensitiveMetric

METRICS = [Accuracy(), SensitiveMetric(Accuracy)]

def get_metrics(dataset, sensitive_dict, tag):
    """
    Takes a dataset object and a dictionary mapping sensitive attributes to a list of the sensitive
    values seen in the data.  Returns an expanded list of metrics based on the base METRICS.
    """
    metrics = []
    for metric in METRICS:
        metrics += metric.expand_per_dataset(dataset, sensitive_dict, tag)
    return metrics

def add_metric(metric):
    METRICS.append(metric)
