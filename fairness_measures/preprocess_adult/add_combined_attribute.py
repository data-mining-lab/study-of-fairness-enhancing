import pandas as pd
import glob

path = ('data')
filenames = glob.glob(path + "/*.csv")


dfs = []
for filename in filenames:
    print(filename)
    data = pd.read_csv(filename)
    combined_attribute = []
    if data.dtypes['income-per-year'] == 'int64':
        for vec in data.iterrows():
            combined_attribute.append(str(int(vec[1]['race-sex'])) + str(int(vec[1]['income-per-year'])))
    else:
        for vec in data.iterrows():
            combined_attribute.append(str(vec[1]['race-sex']) + str(vec[1]['income-per-year']))
    data = data.drop(columns='race-sex')
    data['race-sex-income-per-year'] = combined_attribute
    data.to_csv('modified\\' + filename.strip('data'))
