import pandas as pd
import glob

path = ('data')
filenames = glob.glob(path + "/*.csv")


dfs = []
for filename in filenames:
    print(filename)
    data = pd.read_csv(filename)
    combined_attribute = []
    if data.dtypes['sex-race'] == 'int64':
        for vec in data.iterrows():
            combined_attribute.append(str(int(vec[1]['sex-race'])) + str(int(vec[1]['two_year_recid'])))
    else:
        for vec in data.iterrows():
            combined_attribute.append(str(vec[1]['sex-race']) + str(vec[1]['two_year_recid']))
    data = data.drop(columns='sex-race')
    data['sex-race-two_year_recid'] = combined_attribute
    data.to_csv('modified' + filename.strip('data'))
