import pandas as pd
import glob

path = ('data')
filenames = glob.glob(path + "/*.csv")


dfs = []
for filename in filenames:
    print(filename)
    data = pd.read_csv(filename)
    combined_attribute = []
    if data.dtypes['Race'] == 'int64':
        for vec in data.iterrows():
            combined_attribute.append(str(int(vec[1]['Race'])) + str(int(vec[1]['Class'])))
    else:
        for vec in data.iterrows():
            combined_attribute.append(str(vec[1]['Race']) + str(int(vec[1]['Class'])))
    data['Race-Class'] = combined_attribute
    data.to_csv('modified' + filename.strip('data'))
