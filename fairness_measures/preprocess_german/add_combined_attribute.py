import pandas as pd
import glob

path = ('data')
filenames = glob.glob(path + "/*.csv")


dfs = []
for filename in filenames:
    print(filename)
    data = pd.read_csv(filename)
    combined_attribute = []
    if data.dtypes['sex-age'] == 'int64':
        for vec in data.iterrows():
            combined_attribute.append(str(int(vec[1]['sex-age'])) + str(int(vec[1]['credit'])))
    else:
        for vec in data.iterrows():
            combined_attribute.append(str(vec[1]['sex-age']) + str(vec[1]['credit']))
    data = data.drop(columns='sex-age')
    data['sex-age-credit'] = combined_attribute
    data.to_csv('modified' + filename.strip('data'))
