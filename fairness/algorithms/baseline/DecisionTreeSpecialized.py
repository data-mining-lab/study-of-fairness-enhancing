from sklearn.tree import DecisionTreeClassifier as SKLearn_DT
from algorithms.baseline.GenericSpecialized import GenericSpecialized

class DecisionTreeSpecialized(GenericSpecialized):
    def __init__(self):
        GenericSpecialized.__init__(self)
        self.classifier = SKLearn_DT()
        self.name = "DecisionTreeSpecialized"