from algorithms.Algorithm import Algorithm
import pandas as pd
import numpy as np

class GenericSpecialized(Algorithm):
    def __init__(self):
        Algorithm.__init__(self)
        ## self.classifier should be set in any class that extends this one

    def run(self, train_df, test_df, class_attr, positive_class_val, sensitive_attrs,
            single_sensitive, privileged_vals, params):
        predictions = []
        actual = []

        uniqueAttributes = pd.unique(test_df[single_sensitive])
        np.append(uniqueAttributes, pd.unique(train_df[single_sensitive]))
        for sensitive in uniqueAttributes:
            train = train_df.loc[train_df[single_sensitive] == sensitive]
            test = test_df.loc[test_df[single_sensitive] == sensitive]

            # remove sensitive attributes from the training set
            train_df_nosensitive = train.drop(columns = sensitive_attrs)
            test_df_nosensitive = test.drop(columns = sensitive_attrs)

            # create and train the classifier
            classifier = self.get_classifier()
            y = train_df_nosensitive[class_attr]
            X = train_df_nosensitive.drop(columns = class_attr)
            classifier.fit(X, y)
            # Save class because of changed order
            actual.append(test_df_nosensitive[class_attr].values.tolist())
            # print(actual)

            # get the predictions on the test set
            X_test = test_df_nosensitive.drop(class_attr, axis=1)
            predictions.append(classifier.predict(X_test).tolist())
        actual = [item for sublist in actual for item in sublist]
        predictions = [item for sublist in predictions for item in sublist]
        return predictions, [], actual

    def get_supported_data_types(self):
        return set(["numerical", "numerical-binsensitive"])

    def get_classifier(self):
        """
        Returns the created SKLearn classifier object.
        """
        return self.classifier
